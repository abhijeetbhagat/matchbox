﻿/*  Copyright 2013 abhijeet bhagat
 
    This file is part of Matchbox.

    Matchbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Matchbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Matchbox.  If not, see <http://www.gnu.org/licenses/>.
 */

using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using Matchbox.Container;
using Matchbox.Interfaces;
using System;
using System.Linq;

namespace Matchbox.Utilities
{
    public class JSONConfigReader : IMatchboxConfigReader
    {
        #region IConfigReader Members
        
        /// <summary>
        /// Configures a container from a JSON config file
        /// </summary>
        /// <param name="path">Path of the JSON file containing type information</param>
        /// <param name="container">Container that needs to be configured</param>
        public void ReadFromConfig(string path, MatchboxContainer container)
        {
            using (StreamReader reader = File.OpenText(path))
            {
              JObject sections = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
              foreach (var section in sections)
              {
                  switch (section.Key)
                  {
                      case "mappings":
                          foreach (var type in section.Value)
                          {
                              string typeFrom = ((JProperty)type).Value["type"].ToString();
                              string typeTo = ((JProperty)type).Value["mapTo"].ToString();
                              Type InterfaceType = Type.GetType(typeFrom, true, false);
                              Type ConcreteType = Type.GetType(typeTo, true, false);
                              container.RegisterType(InterfaceType, ConcreteType);
                          }
                          break;
                      case "objects":
                          foreach (var type in section.Value)
                          {
                              string typeFrom = ((JProperty)type).Value["type"].ToString();
                              Type ConcreteType = Type.GetType(typeFrom, true, false);
                              container.RegisterObject(ConcreteType, Activator.CreateInstance(ConcreteType));
                          }
                          break;
                      default:
                          throw new Exception("Unknown key in the configuration file");
                  }
                  
              }
            }
            
        }

        #endregion
    }
}
