﻿/*  Copyright 2013 abhijeet bhagat
 
    This file is part of Matchbox.

    Matchbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Matchbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Matchbox.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;
using Matchbox.Utilities;

namespace Matchbox.Container
{
    public class MatchboxContainer
    {
        #region Private members
        //Could this be used for storing concrete objects against interfaces so that 
        //ResolveType<> doesnt create and return them at runtime but simply returns them?
        Dictionary<Type, object> eagerContainer;

        /// <summary>
        /// Registry of types
        /// </summary>
        Dictionary<Type, Type> lazyContainer; 
        #endregion

        #region Public members
        /// <summary>
        /// Initializes the containers
        /// </summary>
        public MatchboxContainer()
        {
            eagerContainer = new Dictionary<Type, object>();
            lazyContainer = new Dictionary<Type, Type>();
        }

        /// <summary>
        /// Registers interface type and the implementation type with the container.
        /// (Stores the interface type and the implementation instance in the container)
        /// </summary>
        /// <typeparam name="TInterface">Interface</typeparam>
        /// <typeparam name="TImplementation">Implementation</typeparam>
        public void RegisterType<TInterface, TImplementation>() where TImplementation : TInterface
        {
            AddToContainer(typeof(TInterface), typeof(TImplementation));
        }

        /// <summary>
        /// Registers interface type and the implementation type with the container.
        /// (Stores the interface type and the implementation instance in the container)
        /// </summary>
        /// <param name="TFrom">Interface</param>
        /// <param name="TTo">Implementation</param>
        public void RegisterType(Type TFrom, Type TTo)
        {
            AddToContainer(TFrom, TTo);
        }

        /// <summary>
        /// Stores an object of the given type
        /// </summary>
        /// <typeparam name="T">Type of the object</typeparam>
        /// <param name="obj">object of type T</param>
        public void RegisterObject<T>(T obj)
        {
            if (eagerContainer.ContainsKey(typeof(T)))
            {
                eagerContainer[typeof(T)] = obj;
            }
            else
            {
                eagerContainer.Add(typeof(T), obj);
            }
        }


        public void RegisterObject(Type T, object obj)
        {
            if (eagerContainer.ContainsKey(T))
            {
                eagerContainer[T] = obj;
            }
            else
            {
                eagerContainer.Add(T, obj);
            }
        }

        /// <summary>
        /// Gets an object already stored in the container
        /// </summary>
        /// <typeparam name="T">Type of the object requested</typeparam>
        /// <returns>object of type T</returns>
        public T ResolveObject<T>() where T : class
        {
            return eagerContainer[typeof(T)] as T;
        }
        
        // <summary>
        /// Returns the implementation object of TInterface
        /// </summary>
        /// <typeparam name="TInterface">Interface</typeparam>
        /// <returns></returns>
        public TInterface ResolveType<TInterface>() where TInterface : class
        {
            return Resolve(typeof(TInterface)) as TInterface;
        }

        /// <summary>
        /// Returns an object
        /// </summary>
        /// <param name="T"></param>
        /// <returns></returns>
        public object ResolveType(Type T)
        {
            return Resolve(T);
        }

        /// <summary>
        /// Creates an instance of type TImplementation which should implement TInterface.
        /// Warning: If TImplementation doesn't implement TInterface, a null will be returned.
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <typeparam name="TImplementation"></typeparam>
        /// <param name="ctorArgs"></param>
        /// <returns></returns>
        public TInterface ResolveType<TInterface, TImplementation>(params object[] ctorArgs) where TInterface : class
        {
            return Activator.CreateInstance(typeof(TImplementation), ctorArgs) as TInterface;
        }

        /// <summary>
        /// Gets or sets the value associated with the specified type T
        /// </summary>
        /// <param name="T">Type</param>
        /// <returns></returns>
        public object this[Type T]
        {
            get
            {
                return Resolve(T);
            }

            set
            {
                AddToContainer(T, (Type)value);
            }
        }

        /// <summary>
        /// Configures the container from a JSON config file
        /// </summary>
        /// <param name="path">Path of the JSON config file</param>
        public void RegisterTypesFromConfig(string path)
        {
            var reader = new JSONConfigReader();
            reader.ReadFromConfig(path, this);
        } 
        #endregion

        #region Private Members
        /// <summary>
        /// This is used by ResolveType<> and creates concrete objects on the fly
        /// </summary>
        /// <param name="InterfaceType">Interface type for which resolution is to be done</param>
        /// <returns></returns>
        private object Resolve(Type InterfaceType)
        {
            if (lazyContainer.ContainsKey(InterfaceType))
            {
                Type ConcreteType = lazyContainer[InterfaceType];
                ConstructorInfo[] ctors = ConcreteType.GetConstructors();

                //Check if from all the ctors we get atleast one CI attribute
                //There should be only one such ctor marked for CI
                List<object[]> ctorwiseListOfAttributes = ctors.Select(ci => ci.GetCustomAttributes(false)).ToList();
                int countOfCtorsWithCIAttribute = ctorwiseListOfAttributes.Count(attributesList => attributesList.Count(attribute => attribute.GetType() == typeof(DICtor)) == 1);

                if (countOfCtorsWithCIAttribute > 1)
                {
                    throw new Exception("Only one constructor can be marked for constructor injection.");
                }

                //Get the ctor which is marked for CI
                var ctorWithCIAttribute = ctors
                                            .FirstOrDefault(ctorInfo =>
                                                            ctorInfo
                                                            .GetCustomAttributes(false)
                                                            .Count(attribute => attribute.GetType() == typeof(DICtor)) == 1);

                //if there is a constructor which is the only one having the CI attribute,
                //get all its params, resolve their types and create an instance which the
                //constructor represents
                if (ctorWithCIAttribute != null)
                {
                    ParameterInfo[] parameters = ctorWithCIAttribute.GetParameters();
                    //if we are looking at a default constructor thats marked for CI, 
                    //just create an instance of the ConcreteType and return it
                    if (parameters.Length == 0)
                        return Activator.CreateInstance(ConcreteType);

                    return CreateInstanceWithParams(InterfaceType, ConcreteType, parameters);
                }

                //if there is only a default ctor, just create an instance
                if (ctors.Length == 1 && ctors[0].GetParameters().Length == 0)
                {
                    return Activator.CreateInstance(ConcreteType);
                }

                //Check if there are two or more ctors marked for DI
                //Two or more ctors marked for DI is illegal
                var listOfCtorParamsAndParamsLengths = ctors.Select(
                                                    t => new KeyValuePair<int, ParameterInfo[]>(t.GetParameters().Length, t.GetParameters())
                                                    );

                int maxParamCount = listOfCtorParamsAndParamsLengths.Max(kvp => kvp.Key);
                int countOfNumberOfMaxParam = listOfCtorParamsAndParamsLengths.Count(kvp => kvp.Key == maxParamCount);
                if (countOfNumberOfMaxParam >= 2)
                {
                    throw new Exception("Too many constructors with the same number of highest args.");
                }

                //Find the params array with max count
                var ctorParams = listOfCtorParamsAndParamsLengths.First(kvp => kvp.Key == maxParamCount);

                return CreateInstanceWithParams(InterfaceType, ConcreteType, ctorParams.Value);

            }
            else
            {
                throw new Exception(String.Format("Type {0} not registered.", InterfaceType.ToString()));
            }
        }

        /// <summary>
        /// Adds types to the dictionary
        /// </summary>
        /// <param name="TInterface">Interface</param>
        /// <param name="TImplementation">Implementation</param>
        private void AddToContainer(Type TInterface, Type TImplementation)
        {
            //let TImplementation not be an interface type i.e. RegisterType<IFoo, IFoo>() is illegal
            if (TImplementation.IsAbstract || TImplementation.IsInterface)
            {
                throw new Exception("TImplementation should be instantiable.");
            }

            //Ensure that on adding an existing key, it's value is replaced
            if (lazyContainer.ContainsKey(TInterface))
            {
                lazyContainer[TInterface] = TImplementation;
            }
            else
            {
                lazyContainer.Add(TInterface, TImplementation);
            }
        }

        /// <summary>
        /// Creates an object by calling its ctor with the parameters.
        /// </summary>
        /// <param name="InterfaceType">Interface</param>
        /// <param name="ConcreteType">Implementation</param>
        /// <param name="parameters">ctor parameters</param>
        /// <returns></returns>
        private object CreateInstanceWithParams(Type InterfaceType, Type ConcreteType, ParameterInfo[] parameters)
        {
            object[] parameterInstances = new object[parameters.Length];
            for (int i = 0; i < parameterInstances.Length; i++)
            {
                if (lazyContainer.ContainsKey(parameters[i].ParameterType))
                {
                    Type T = parameters[i].ParameterType;
                    //Recursively resolve the dependencies that are in the params
                    parameterInstances[i] = Resolve(T);
                }
                else
                    throw new Exception(String.Format("Type {0} not registered.", parameters[i].ParameterType.ToString()));
            }
            return Activator.CreateInstance(ConcreteType, parameterInstances);
        } 
        #endregion
    }

    /// <summary>
    /// Class that indicates which ctor is to be used for dependency injection
    /// </summary>
    [AttributeUsage(AttributeTargets.Constructor)]
    public class DICtor : Attribute { }

}
