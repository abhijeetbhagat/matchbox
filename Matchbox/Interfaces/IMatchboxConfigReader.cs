﻿/*  Copyright 2013 abhijeet bhagat
 
    This file is part of Matchbox.

    Matchbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Matchbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Matchbox.  If not, see <http://www.gnu.org/licenses/>.
 */

using Matchbox.Container;

namespace Matchbox.Interfaces
{
    /// <summary>
    /// Interface to be implemented by config readers
    /// </summary>
    public interface IMatchboxConfigReader
    {
        void ReadFromConfig(string path, MatchboxContainer container);
    }
 }
