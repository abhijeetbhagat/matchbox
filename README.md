This is an experimental DI container.

	using Matchbox.Container;
	using System;

	interface IFoo { void foo();}
    class Foo : IFoo
    {
        public Foo()
        {
            Console.WriteLine("Foo::Foo()");
        }

        IBar bar;

        [DICtor]
        public Foo(IBar bar)
        {
            Console.WriteLine("Foo::Foo(IBar bar)");
            this.bar = bar;
        }

        public Foo(IBar bar, int i)
        {
            Console.WriteLine("i is {0}", i);
            this.bar = bar;
        }

        public void foo()
        {
            Console.WriteLine("foo!");
            bar.bar();
        }
    }

    interface IBar { void bar();}
    class Bar : IBar
    {
        public void bar()
        {
            Console.WriteLine("bar!");
        }
    }

    interface IQux { void qux(); }
    class Qux : IQux
    {
        public Qux()
        {
            Console.WriteLine("Qux::Qux()");
        }

        IFoo foo;
        IBar bar;

        [DICtor]
        public Qux(IFoo foo, IBar bar)
        {
            this.foo = foo;
            this.bar = bar;
        }

        public void qux()
        {
            Console.WriteLine("qux!");
            foo.foo();
            bar.bar();
        }
    }

	try
    {
        MatchboxContainer m = new MatchboxContainer();
                
        m.RegisterType<IBar, Bar>();
        m.RegisterType<IBar, Bar>();
        m.RegisterType<IFoo, Foo>();
        m.RegisterType<Bar, Bar>();
        m.RegisterType<IQux, Qux>();
                
        Bar b = m.ResolveType<Bar>();
        b.bar();

        IFoo f = m.ResolveType<IFoo>();
        f.foo();

        IBar ib = m.ResolveType<IBar>();
        ib.bar();

        IQux q = m.ResolveType<IQux>();
        q.qux();

        IFoo f2 = m.ResolveType<IFoo, Foo>(new Bar(), 1);
        f2.foo();

        //m.RegisterType<IQux, IQux>(); Illegal
        IQux q2 = m.ResolveType<IQux, Foo>(new Bar());
        q2.qux(); //null reference exception

    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
    }

Output:

	bar!
	Foo::Foo(IBar bar)
	foo!
	bar!
	bar!
	Foo::Foo(IBar bar)
	qux!
	foo!
	bar!
	bar!
	i is 1
	foo!
	bar!
	Foo::Foo(IBar bar)
	Object reference not set to an instance of an object.