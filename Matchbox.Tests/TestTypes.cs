﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchbox.Container;

namespace Matchbox.Tests
{
    public interface IFoo { void foo();}
    public class Foo2 : IFoo
    {
        #region IFoo Members

        public void foo()
        {
            throw new NotImplementedException();
        }

        #endregion
    }


    public class Foo : IFoo
    {
        public Foo()
        {
            Console.WriteLine("Foo::Foo()");
        }

        IBar bar;

        [DICtor]
        public Foo(IBar bar)
        {
            Console.WriteLine("Foo::Foo(IBar bar)");
            this.bar = bar;
        }

        public Foo(IBar bar, int i)
        {
            Console.WriteLine("i is {0}", i);
            this.bar = bar;
        }

        public void foo()
        {
            Console.WriteLine("foo!");
            bar.bar();
        }
    }

    public interface IBar { void bar();}
    public class Bar : IBar
    {
        public void bar()
        {
            Console.WriteLine("bar!");
        }
    }

    public interface IQux { void qux(); }
    public class Qux : IQux
    {
        public Qux()
        {
            Console.WriteLine("Qux::Qux()");
        }

        IFoo foo;
        IBar bar;

        [DICtor]
        public Qux(IFoo foo, IBar bar)
        {
            this.foo = foo;
            this.bar = bar;
        }

        public void qux()
        {
            Console.WriteLine("qux!");
            foo.foo();
            bar.bar();
        }
    }
}
