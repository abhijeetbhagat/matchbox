﻿using NUnit.Framework;
using System;
using Matchbox.Container;

namespace Matchbox.Tests
{
    [TestFixture]
    public class MatchboxContainerTests
    {
        [Test]
        public void CtorTest()
        {
            MatchboxContainer c = new MatchboxContainer();
            Assert.IsNotNull(c, "container creation failed");
        }


        [Test]
        public void RegisterObjectTest()
        {
            MatchboxContainer c = new MatchboxContainer();
            Foo f1= new Foo();
            Foo f2 = new Foo();

            c.RegisterObject<IFoo>(f1);
            c.RegisterObject<IFoo>(f2);

            Assert.AreSame(f1, c.ResolveObject<IFoo>(), "Objects are not same");
            Console.ReadKey();
        }

        [Test]
        public void ResolveTypeGenericVersionTest()
        {
            MatchboxContainer c = new MatchboxContainer();
            c.RegisterType<IFoo, Foo>();
            c.RegisterType<IBar, Bar>();
            IFoo f = c.ResolveType<IFoo>();

            Assert.IsNotNull(f, "Object null");
        }

        [Test]
        public void RegisterTypesFromConfigTest()
        {
            MatchboxContainer c = new MatchboxContainer();
            c.RegisterTypesFromConfig(@"E:\c0d3\w1nd0z\CSharp\Matchbox\Matchbox\Utilities\test.json");
            IFoo f = c.ResolveType<IFoo>();
            Assert.IsNotNull(f, "Object null");
        }

        [Test]
        public void RegisterObject_Type_obj_Test()
        {
            MatchboxContainer c = new MatchboxContainer();
            c.RegisterTypesFromConfig(@"E:\c0d3\w1nd0z\CSharp\Matchbox\Matchbox\Utilities\test.json");
            IFoo f = c.ResolveObject<Foo>();
            Assert.IsNotNull(f, "Object null");
        }
    }
}
